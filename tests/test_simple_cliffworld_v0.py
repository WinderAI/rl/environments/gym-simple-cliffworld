"""Test simple_cliffworld_v0.py."""
import gym  # type: ignore

from gym_simple_cliffworld.envs import simple_cliffworld_v0


def test_registration() -> None:
    """Ensure the env registers."""
    env = gym.make("SimpleCliffworld-v0")
    assert env is not None


def test_fall_off_cliff() -> None:
    """When the agent falls off the cliff return -1."""
    env = gym.make("SimpleCliffworld-v0")
    done = False
    reward = 0
    while not done:
        _, reward, done, _ = env.step(simple_cliffworld_v0.LEFT)
    assert reward == 0


def test_success() -> None:
    """When the agent succeeds reward +1."""
    env = gym.make("SimpleCliffworld-v0")
    done = False
    reward = 0
    while not done:
        _, reward, done, _ = env.step(simple_cliffworld_v0.RIGHT)
    assert reward == 5


def test_state_is_not_empty() -> None:
    """State should not be empty."""
    env = gym.make("SimpleCliffworld-v0")
    state, _, _, _ = env.step(simple_cliffworld_v0.RIGHT)
    assert isinstance(state, int)
